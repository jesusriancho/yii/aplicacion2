<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionEjercicio1()
    {
        $alumnos = [
            'jorge',
            'pedro',
            'maria',
            'laura',
            'marcos'
        ];
        return $this->render('ejercicio1', [
            'variable' => $alumnos
        ]);
    }

    public function actionEjercicio2a()
    {
        $alumnos = [
            'foto (1).jpeg',
            'foto (2).jpeg',
            'foto (3).jpeg',
            'foto (4).jpeg'
        ];

        return $this->render('ejercicio2a', [
            'imagenes' => $alumnos
        ]);
    }

    public function actionEjercicio2b()
    {
        $alumnos = [
            'foto (1).jpeg',
            'foto (2).jpeg',
            'foto (3).jpeg',
            'foto (4).jpeg'
        ];

        return $this->render('ejercicio2b', [
            'imagenes' => $alumnos
        ]);
    }

    public function actionEjercicio3($id = 0)
    {
        $alumnos = [
            [
                "nombre" => "Jorge",
                "poblacion" => "Santander",
                "imagen" => "foto (1).jpeg"
            ],
            [
                "nombre" => "Pedro",
                "poblacion" => "Cantabria",
                "imagen" => "foto (2).jpeg"
            ],
            [
                "nombre" => "Maria",
                "poblacion" => "Cantabria",
                "imagen" => "foto (3).jpeg"
            ],
            [
                "nombre" => "Laura",
                "poblacion" => "Cantabria",
                "imagen" => "foto (4).jpeg"
            ]
        ];

        $alumnoSinFoto = $alumnos[$id];
        // quito la imagen en el array alumnoSinFoto para el widget
        unset($alumnoSinFoto["imagen"]);

        return $this->render('ejercicio3', [
            "alumno" => $alumnos[$id],
            "alumnoSinFoto" => $alumnoSinFoto
        ]);
    }


    public function actionEjercicio4()
    {
        $numeros = [
            1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20
        ];
        return $this->render('ejercicio4', [
            'numeros' => $numeros
        ]);
    }

    public function actionEjercicio5()
    {
        $datos = [
            [
                "id" => 1,
                "nombre" => "Jorge",
                "poblacion" => "Santander",
                "direccion" => "calle de abajo"
            ],
            [
                "id" => 2,
                "nombre" => "Pedro",
                "poblacion" => "Cantabria",
                "direccion" => "calle de arriba"
            ],
            [
                "id" => 3,
                "nombre" => "Maria",
                "poblacion" => "Cantabria",
                "direccion" => "calle de arriba"
            ],
            [
                "id" => 4,
                "nombre" => "Laura",
                "poblacion" => "Cantabria",
                "direccion" => "calle de arriba"
            ]
        ];

        // quiero que en la vista se muestren los datos anteriores
        // utilizando CARDS
        // en el titulo de la CARD me coloca el id de cada elemento
        // en el texto de la CARD me coloca el nombre
        // el resto de los campos en una lista dentro de la CARD
        // con el formato NombreCampo: valor

        return $this->render('ejercicio5', [

            'datos' => $datos
        ]);
    }

    public function actionEjercicio6()
    {
        $datos = [
            [
                "id" => 1,
                "nombre" => "Jorge",
                "poblacion" => "Santander",
                "direccion" => "calle de abajo"
            ],
            [
                "id" => 2,
                "nombre" => "Pedro",
                "poblacion" => "Cantabria",
                "direccion" => "calle de arriba"
            ],
            [
                "id" => 3,
                "nombre" => "Maria",
                "poblacion" => "Cantabria",
                "direccion" => "calle de arriba"
            ],
            [
                "id" => 4,
                "nombre" => "Laura",
                "poblacion" => "Cantabria",
                "direccion" => "calle de arriba"
            ]
        ];

        // quiero que en la vista se muestren los datos anteriores
        // utilizando CARDS
        // en el titulo de la CARD me coloca el id de cada elemento
        // en el texto de la CARD me coloca el nombre
        // el resto de los campos en una lista dentro de la CARD
        // con el formato NombreCampo: valor
        // utilizar subvistas (realizar render dentro de la vista)

        return $this->render('ejercicio6', [
            'datos' => $datos
        ]);
    }

}
