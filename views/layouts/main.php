<?php

/** @var yii\web\View $this */
/** @var string $content */

use app\assets\AppAsset;
use app\widgets\Alert;
use yii\bootstrap5\Breadcrumbs;
use yii\bootstrap5\Html;
use yii\bootstrap5\Nav;
use yii\bootstrap5\NavBar;

AppAsset::register($this);

$this->registerCsrfMetaTags();
$this->registerMetaTag(['charset' => Yii::$app->charset], 'charset');
$this->registerMetaTag(['name' => 'viewport', 'content' => 'width=device-width, initial-scale=1, shrink-to-fit=no']);
$this->registerMetaTag(['name' => 'description', 'content' => $this->params['meta_description'] ?? '']);
$this->registerMetaTag(['name' => 'keywords', 'content' => $this->params['meta_keywords'] ?? '']);
$this->registerLinkTag(['rel' => 'icon', 'type' => 'image/x-icon', 'href' => Yii::getAlias('@web/favicon.ico')]);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" class="h-100">

<head>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>

<body class="d-flex flex-column h-100">
    <?php $this->beginBody() ?>

    <header id="header">
        <?php
        NavBar::begin([
            'brandLabel' => Yii::$app->name,
            'brandUrl' => Yii::$app->homeUrl,
            'options' => ['class' => 'navbar-expand-md navbar-dark bg-dark fixed-top']
        ]);
        echo Nav::widget([
            'options' => ['class' => 'navbar-nav'],
            'items' => [
                ['label' => 'Home', 'url' => ['/site/index']],
                ['label' => 'Ejercicio 1', 'url' => ['/site/ejercicio1']],
                ['label' => 'Ejercicio 2', 'items' => [
                    ['label' => 'Ejercicio 2 - Foreach', 'url' => ['/site/ejercicio2a']],
                    ['label' => 'Ejercicio 2 - Ul', 'url' => ['/site/ejercicio2b']],
                ]],
                ['label' => 'Ejercicio 3', 'items' => [
                    ['label' => 'Ejercicio 3 - Alumno 1', 'url' => ['/site/ejercicio3', 'id' => 0]],
                    ['label' => 'Ejercicio 3 - Alumno 2', 'url' => ['/site/ejercicio3', 'id' => 1]],
                    ['label' => 'Ejercicio 3 - Alumno 3', 'url' => ['/site/ejercicio3', 'id' => 2]],
                    ['label' => 'Ejercicio 3 - Alumno 4', 'url' => ['/site/ejercicio3', 'id' => 3]],
                ]],
                ['label' => 'Ejercicio 4', 'url' => ['/site/ejercicio4']],
                ['label' => 'Ejercicio 5', 'url' => ['/site/ejercicio5']],
                ['label' => 'Ejercicio 6', 'url' => ['/site/ejercicio6']],
            ]
        ]);
        NavBar::end();
        ?>
    </header>

    <main id="main" class="flex-shrink-0" role="main">
        <div class="container">
            <?php if (!empty($this->params['breadcrumbs'])) : ?>
                <?= Breadcrumbs::widget(['links' => $this->params['breadcrumbs']]) ?>
            <?php endif ?>
            <?= Alert::widget() ?>
            <?= $content ?>
        </div>
    </main>

    <footer id="footer" class="mt-auto py-3 bg-light">
        <div class="container">
            <div class="row text-muted">
                <div class="col-md-6 text-center text-md-start">&copy; My Company <?= date('Y') ?></div>
                <div class="col-md-6 text-center text-md-end"><?= Yii::powered() ?></div>
            </div>
        </div>
    </footer>

    <?php $this->endBody() ?>
</body>

</html>
<?php $this->endPage() ?>
