<?php

echo $this->render('_listado', [
    'numeros' => $numeros
]);

// sin crear vistas nuevas
?>
<br><br>
<ul class="list-group list-group-horizontal">
    <?php
    foreach ($numeros as $numero) {
        echo '<li class="list-group-item">' . $numero . '</li>';
    }
    ?>
</ul>
<br>
<br>
<?php

// con el helper Html::ul

use yii\helpers\Html;

echo Html::ul($numeros, [
    'class' => 'list-group list-group-horizontal',
    'itemOptions' => [
        'class' => 'list-group-item'
    ]
]);
