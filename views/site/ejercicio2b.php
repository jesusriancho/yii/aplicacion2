<?php

use yii\helpers\Html;


echo Html::ul($imagenes, [
    'class' => 'list-group list-group-horizontal',  // colocar una clase a la etiqueta ul
    'item' => function ($foto, $indice) {
        $salida = "<li class='list-group-item col-lg-3'>";
        $salida .= Html::img("@web/imgs/{$foto}", [
            'class' => 'img-thumbnail',
        ]);
        $salida .= "</li>";
        return $salida;
    },
]);
