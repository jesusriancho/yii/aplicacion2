<?php

// sin utilizar Helpers de HTML

use yii\helpers\Html;

?>

<div class="row">
    <div class="col-sm-3">
        <div class="card">
            <img src="<?= Yii::getAlias("@web") ?>/imgs/<?= $alumno["imagen"] ?>">
            <div class="card-body">
                <p>Nombre: <?= $alumno["nombre"] ?></p>
                <p>Poblacion: <?= $alumno["poblacion"] ?></p>

            </div>
        </div>
    </div>
    <div class="col-sm-3">
        <div class="card">
            <img src="<?= Yii::getAlias("@web") ?>/imgs/<?= $alumno["imagen"] ?>">
            <ul class="list-group list-group-flush">
                <li class="list-group-item">Nombre: <?= $alumno["nombre"] ?></li>
                <li class="list-group-item">Poblacion: <?= $alumno["poblacion"] ?></li>
            </ul>

        </div>
    </div>
</div>

<?php

// quiero utilizar los helpers de HTML

?>
<div class="row">
    <div class="col-sm-3">
        <div class="card">
            <?= Html::img("@web/imgs/{$alumno["imagen"]}") ?>
            <?= Html::ul($alumnoSinFoto, [
                "class" => "list-group list-group-flush",
                "itemOptions" => [
                    "class" => "list-group-item"
                ],
                // "item" => function ($dato, $indice) {
                //     $salida = "";
                //     if ($indice != "imagen") {
                //         $salida .= "<li class='list-group-item'>{$indice}: {$dato}</li>";
                //     }
                //     return $salida;
                // }
            ]) ?>
        </div>
    </div>
</div>
