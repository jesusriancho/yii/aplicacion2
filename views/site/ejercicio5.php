<?php

// sin utilizar Helpers de HTML

use yii\helpers\Html;

?>

<div class="row">
    <?php
    foreach ($datos as $dato) {
    ?>
        <div class="col-sm-3">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Id: <?= $dato["id"] ?></h3>
                </div>
                <div class="card-body">
                    <p class="card-text">Nombre: <?= $dato["nombre"] ?></p>
                </div>
                <ul class=" list-group list-group-flush">
                    <li class="list-group-item">Poblacion: <?= $dato["poblacion"] ?></li>
                    <li class="list-group-item">Direccion: <?= $dato["direccion"] ?></li>
                </ul>
            </div>
        </div>
    <?php
    }
    ?>
</div>
